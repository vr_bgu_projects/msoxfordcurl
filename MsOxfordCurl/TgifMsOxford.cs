﻿namespace MsOxfordCurl
{
    public class TgifMsOxfordAnalyzeResult
    {
        public string image_id { get; set; }
        public string image_url { get; set; }
        public string msOxfordResult { get; set; }

        public TgifMsOxfordAnalyzeResult(string id, string url, string result)
        {
            image_id = id;
            image_url = url;
            msOxfordResult = result;     
        }


    }

    public class TgifMsOxfordDescribeResult
    {
        public string image_id { get; set; }
        public string image_url { get; set; }
        public string msOxfordResult { get; set; }

        public TgifMsOxfordDescribeResult(string id, string url, string result)
        {
            image_id = id;
            image_url = url;
            msOxfordResult = result;
        }


    }


    public class TgifMsOxfordTagResult
    {
        public string image_id { get; set; }
        public string image_url { get; set; }
        public string msOxfordResult { get; set; }

        public TgifMsOxfordTagResult(string id, string url, string result)
        {
            image_id = id;
            image_url = url;
            msOxfordResult = result;
        }


    }
}