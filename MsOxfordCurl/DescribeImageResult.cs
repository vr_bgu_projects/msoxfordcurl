﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsOxfordCurl
{

    public class Caption
    {
        public string text { get; set; }
        public double confidence { get; set; }
    }

    public class Description
    {
        public List<string> tags { get; set; }
        public List<Caption> captions { get; set; }
    }
    [JsonObject("metadata")]
    public class MetadataDescribeImageResult
    {
        public int width { get; set; }
        public int height { get; set; }
        public string format { get; set; }
    }

    public class DescribeImageResult
    {
        public Description description { get; set; }
        public string requestId { get; set; }
        public Metadata metadata { get; set; }
    }
}
