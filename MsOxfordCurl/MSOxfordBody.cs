﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MsOxfordCurl
{
    public class MSOxfordBody
    {
        public MSOxfordBody(string imageUrl)
        {
            this.url = imageUrl;
        }

        public string url { get; set; }
    }
}