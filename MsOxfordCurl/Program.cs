﻿using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace MsOxfordCurl
{
    class Program
    {
        public object Util { get; private set; }

        static void Main(string[] args)
        {
            string messageToUser = "first arg is api key, second arg is file with [id,iamge url] in new line, third arg is string with the following chars [a,b,c], a indiciting Analyze Image, B describe Image, C Tag Image, D ocr, E Thumbnail ";
            Console.WriteLine(messageToUser);

            if (args.Length != 3)
            {
                Console.WriteLine(messageToUser);
                Environment.Exit(0);
            }
            var lines = File.ReadAllLines(args[1]);
            string options = args[2];

            List<TgifMsOxfordAnalyzeResult> resultsAnalyze = new List<TgifMsOxfordAnalyzeResult>();
            List<TgifMsOxfordDescribeResult> resultsDescribe = new List<TgifMsOxfordDescribeResult>();

            List<TgifMsOxfordTagResult> resultsTag = new List<TgifMsOxfordTagResult>();

            System.IO.TextWriter writeFileAnalyze = new StreamWriter("TgifMsOxfordAnalyzeResults.csv");
            System.IO.TextWriter writeFileDescribe = new StreamWriter("TgifMsOxfordDescribeResults.csv");
            var csvAnalyze = new CsvWriter(writeFileAnalyze);
            System.IO.TextWriter writeFileTag = new StreamWriter("TgifMsOxfordTagResults.csv");
            var csvTag = new CsvWriter(writeFileTag);
            var csvDescribe = new CsvWriter(writeFileDescribe);


            foreach (var line in lines)
            {
                try
                {
                    string id = line.Split(',')[0];
                    string image_url = line.Split(',')[1];
                    Console.WriteLine("Image Id: " + id + ", Image url: " + image_url);


                    if (options.Contains("a"))
                    {
                        AnalysisResult analysisResult = AnalyzeImageCurl(args[0], image_url, "categories,tags,description,faces,imagetype,color,adult");
                        if (analysisResult != null)
                        {
                            string jsonString = JsonConvert.SerializeObject(analysisResult);
                            File.WriteAllText(id + "_analyze.txt", jsonString);
                            TgifMsOxfordAnalyzeResult tgifMsOxfordAnalyzeResult = new TgifMsOxfordAnalyzeResult(id, image_url, jsonString);
                            resultsAnalyze.Add(tgifMsOxfordAnalyzeResult);
                            csvAnalyze.WriteRecord(tgifMsOxfordAnalyzeResult);
                            writeFileAnalyze.Flush();

                            int milliseconds = 3500;
                            Thread.Sleep(milliseconds);
                        }
                    }


                    if (options.Contains("b"))
                    {
                        DescribeImageResult describeImageResult = DescribeImageCurl(args[0], image_url, "5");
                        if (describeImageResult != null)
                        {
                            string jsonString = JsonConvert.SerializeObject(describeImageResult);
                            File.WriteAllText(id + "_describe.txt", jsonString);
                            TgifMsOxfordDescribeResult tgifMsOxfordDescribeResult = new TgifMsOxfordDescribeResult(id, image_url, jsonString);
                            resultsDescribe.Add(tgifMsOxfordDescribeResult);
                            csvDescribe.WriteRecord(tgifMsOxfordDescribeResult);
                            writeFileDescribe.Flush();

                            int milliseconds = 3500;
                            Thread.Sleep(milliseconds);
                        }
                    }

                    if (options.Contains("c"))
                    {
                        TagImageResult tagImageResult = TagImageCurl(args[0], image_url);
                        if (tagImageResult != null)
                        {
                            string jsonString = JsonConvert.SerializeObject(tagImageResult);
                            File.WriteAllText(id + "_tag.txt", jsonString);
                            TgifMsOxfordTagResult tgifMsOxfordTagResult = new TgifMsOxfordTagResult(id, image_url, jsonString);
                            resultsTag.Add(tgifMsOxfordTagResult);
                            csvTag.WriteRecord(tgifMsOxfordTagResult);
                            writeFileTag.Flush();

                            int milliseconds = 3500;
                            Thread.Sleep(milliseconds);
                        }
                    }
                    //break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    continue;
                }
            }

            

            //csvAnalyze.WriteRecords(resultsAnalyze);
            writeFileAnalyze.Flush();
            writeFileAnalyze.Close();




            //csvDescribe.WriteRecords(resultsDescribe);
            writeFileDescribe.Flush();
            writeFileDescribe.Close();


            //csvTag.WriteRecords(resultsTag);
            writeFileTag.Flush();
            writeFileTag.Close();
        }

        private static TagImageResult TagImageCurl(string APIKey, string image_url)
        {
            TagImageResult tagImageResponse = null;
            string result = "";
            try
            {
                string body = "{'url':'" + image_url + "'}";

                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/vision/v1.0/tag\" -H \"Content-Type: application/json\" -H \"Ocp-Apim-Subscription-Key: {0}\" --data-ascii \"{1}\"",  APIKey, body);

                stopwatch.Start();
                result = RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();

                tagImageResponse = JsonConvert.DeserializeObject<TagImageResult>(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return tagImageResponse;
        }

        public static AnalysisResult AnalyzeImageCurl(string APIKey, string imageUrl, string visualFeatures)
        {

            AnalysisResult analyzeImageResponse = null;
            string result = "";
            try
            {
                //curl -v -X POST "https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures=Categories&details={string}&language=en"
                //-H "Content-Type: application/json"
                //-H "Ocp-Apim-Subscription-Key: {subscription key}"

                //--data-ascii "{body}"

                string visualFeaturesList = string.Join(",", visualFeatures);
                string body = "{'url':'" + imageUrl + "'}";

                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures={0}&language=en\" -H \"Content-Type: application/json\" -H \"Ocp-Apim-Subscription-Key: {1}\" --data-ascii \"{2}\"", visualFeaturesList, APIKey, body);

                stopwatch.Start();
                result = RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();
                //Console.WriteLine(String.Format("Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds));

                analyzeImageResponse = JsonConvert.DeserializeObject<AnalysisResult>(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return analyzeImageResponse;
        }


        public static DescribeImageResult DescribeImageCurl(string APIKey, string imageUrl, string maxCandidates)
        {
            DescribeImageResult describeImageResponse = null;
            string result = "";
            try
            {
                if (string.IsNullOrEmpty(maxCandidates))
                {
                    maxCandidates = "1";
                }

                string body = "{'url':'" + imageUrl + "'}";

                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/vision/v1.0/describe?maxCandidates={0}\" -H \"Content-Type: application/json\" -H \"Ocp-Apim-Subscription-Key: {1}\" --data-ascii \"{2}\"", maxCandidates, APIKey, body);

                stopwatch.Start();
                result = RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();
                //Console.WriteLine(String.Format("Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds));

                describeImageResponse = JsonConvert.DeserializeObject<DescribeImageResult>(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return describeImageResponse;
        }




        public static string RunProcess(string processFileName, string arguments)
        {
            StringBuilder result = new StringBuilder();

            try
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = processFileName,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                proc.Start();
                stopwatch.Stop();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    result.AppendLine(line);
                    Console.WriteLine(line);
                }
                Console.WriteLine(string.Format("Elapsed Time (ms): {0}", stopwatch.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result.ToString();
        }



    }

    //public sealed class MyClassMap : CsvClassMap<TgifMsOxfordResult>
    //{
    //    public MyClassMap()
    //    {
    //        Map(m => m.Id);
    //        Map(m = > m.Name);
    //    }
    //}
}
