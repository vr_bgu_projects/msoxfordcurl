﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MsOxfordCurl
{

    public class Tag
    {
        public string name { get; set; }
        public double confidence { get; set; }
    }

    [JsonObject("metadata")]
    public class MetadataTagImageResult
    {
        public int width { get; set; }
        public int height { get; set; }
        public string format { get; set; }
    }

    public class TagImageResult
    {
        public List<Tag> tags { get; set; }
        public string requestId { get; set; }
        public Metadata metadata { get; set; }
    }
}